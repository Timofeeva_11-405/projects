//
//  TableViewCellIndicator.h
//  Lesson2
//
//  Created by Мария Тимофеева on 08/10/15.
//  Copyright © 2015 Azat Almeev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellIndicator : UITableViewCell

@property(nonatomic) UIApplication *indicator;

@end
