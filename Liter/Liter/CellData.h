//
//  CellData.h
//  Liter
//
//  Created by Мария Тимофеева on 11.09.15.
//  Copyright (c) 2015 ___matim___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CellData : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *imageName;

+ (NSArray *)fetchData;
+ (NSArray *)fetchData1;


@end
