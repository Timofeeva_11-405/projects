//
//  DetailViewController.h
//  Liter
//
//  Created by Мария Тимофеева on 11.09.15.
//  Copyright (c) 2015 ___matim___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellData.h"

@interface DetailViewController : UIViewController

@property (nonatomic, strong) CellData *detail;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
