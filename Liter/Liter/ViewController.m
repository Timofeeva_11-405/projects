//
//  ViewController.m
//  Liter
//
//  Created by Мария Тимофеева on 11.09.15.
//  Copyright (c) 2015 ___matim___. All rights reserved.
//

#import "ViewController.h"
#import "ImageCell.h"
#import "CellData.h"
#import "DetailViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _data = [CellData fetchData];
    _data1 = [CellData fetchData1 ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return [_data count];

}
- (NSInteger)tableView1:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    
    return [_data1 count];
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *const ImageCellId = @"ImageCell";
    ImageCell *cell = [tableView dequeueReusableCellWithIdentifier:ImageCellId];
    CellData *item = [_data objectAtIndex:indexPath.row];
    cell.cellImage.image = [UIImage imageNamed:item.imageName];
    cell.cellTextLabel.text = item.title;
    return cell;
    
}

//- (UITableViewCell *)tableView :(UITableView *)tableView1 cellForRowAtIndexPath :(NSIndexPath *)indexPath
//{
//static NSString *const ImageCellID = @"ImageCell";
//    ImageCell *cell1 = [tableView1 dequeueReusableCellWithIdentifier:ImageCellID];
//    CellData *item1 = [_data1 objectAtIndex:indexPath.row ];
//    cell1.cellImage1.image = [UIImage imageNamed:item1.imageName];
//    cell1.cellTextLabel1.text = item1.title;
//    return cell1;
//}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if (indexPath) {
    CellData *item  =[_data objectAtIndex:indexPath.row];
        
        [segue.destinationViewController setDetail:item];
    }
    
}


@end
