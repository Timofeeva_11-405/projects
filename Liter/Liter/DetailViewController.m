//
//  DetailViewController.m
//  Liter
//
//  Created by Мария Тимофеева on 11.09.15.
//  Copyright (c) 2015 ___matim___. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setDetail:(CellData *)detail
{
    _detail  = detail;
}

- (void) reloadData
{
    self.textView.text = _detail.text;
    self.imageView.image = [UIImage imageNamed:_detail.imageName];

    CGRect  contentViewFrame = _contentView.frame;
    contentViewFrame.size.height += _textView.contentSize.height - _textView.frame.size.height;
    _contentView.frame = contentViewFrame;
    _scrollView.contentSize = _contentView.frame.size;


}




@end
