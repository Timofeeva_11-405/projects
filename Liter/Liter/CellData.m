//
//  CellData.m
//  Liter
//
//  Created by Мария Тимофеева on 11.09.15.
//  Copyright (c) 2015 ___matim___. All rights reserved.
//

#import "CellData.h"

@implementation CellData

+ (NSArray *)fetchData
{
    NSMutableArray *result = [NSMutableArray array];
    
    CellData *item;
    
    item = [[CellData alloc] init];
    item.title = @"Жуковский В.А.";
    item.text = @"Жуковский Василий Андреевич (1783–1852) – поэт, переводчик, критик, один из основоположников романтизма в русской поэзии. Автор романтических баллад и сентиментальных стихотворений, а также первого официального гимна России; наставник Александра II.";
    item.imageName = @"1.jpg";
    [result addObject:item];
    
    item = [[CellData alloc] init];
    item.title = @"Кюхельбекер В.К.";
    item.text = @"Кюхельбекер Вильгельм Карлович (1797–1846) – русский поэт, писатель и общественный деятель, друг и одноклассник Пушкина по Царскосельскому лицею, декабрист. Публиковал стихи в различных журналах, издавал совместно с А.С. Грибоедовым и В.Ф. Одоевским альманах «Мнемозина».";
    item.imageName = @"2.jpg";
    [result addObject:item];
    
    item = [[CellData alloc] init];
    item.title = @"Лермонтов М.Ю.";
    item.text = @"Лермонтов Михаил Юрьевич (1814–1841) – русский поэт, прозаик, драматург, художник. Творчество Лермонтова, в котором удачно сочетались гражданские, философские и личные мотивы, ознаменовало собой новый расцвет русской литературы.";
    item.imageName = @"3.jpg";
    [result addObject:item];
    
    item = [[CellData alloc] init];
    item.title = @"Пушкин А.С.";
    item.text = @"Пушкин Александр Сергеевич (1799–1837) – великий русский поэт, драматург и прозаик. Один из создателей современного русского литературного языка.";
    item.imageName = @"4.jpg";
    [result addObject:item];
    

    return result;
    
}

+ (NSArray *)fetchData1
{
    NSMutableArray *result = [NSMutableArray array];
    
    CellData *item;
    
    item = [[CellData alloc] init];
    item.title = @"Белый А.";
    item.text = @"Андрей Белый (настоящее имя Борис Николаевич Бугаев; 1880–1934) – русский писатель, поэт, критик, литературовед; один из ведущих деятелей русского символизма.";
    item.imageName = @"5.jpg";
    [result addObject:item];

    
    item = [[CellData alloc] init];
    item.title = @"Блок А.А.";
    item.text = @"Блок Александр Александрович (1880–1921) – поэт, классик русской литературы XX столетия, представитель символизма. Для его творчества характерно парадоксальное сочетание мистического и бытового, отрешённого и повседневного.";
    item.imageName = @"6.jpg";
    [result addObject:item];

    
    item = [[CellData alloc] init];
    item.title = @"Есенин С.А.";
    item.text = @"Есенин Сергей Александрович (1895–1925) – русский поэт, представитель новокрестьянской поэзии, позднее – имажинизма. Выступал как тонкий лирик, мастер глубоко психологизированного пейзажа, певец крестьянской Руси, знаток народного языка и народной души.";
    item.imageName = @"7.jpg";
    [result addObject:item];

    
    item = [[CellData alloc] init];
    item.title = @"Хлебников В.";
    item.text = @"Велимир Хлебников (1885–1922) – поэт и прозаик эпохи Серебряного века, видный деятель русского авангардного искусства. Входил в число основоположников русского футуризма; реформатор поэтического языка, экспериментатор в области словотворчества и «зауми».";
    item.imageName = @"8.jpg";
    [result addObject:item];

    
    return  result;
}

@end
