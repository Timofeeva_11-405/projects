//
//  ImageCell.h
//  Liter
//
//  Created by Мария Тимофеева on 11.09.15.
//  Copyright (c) 2015 ___matim___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;

@property (weak, nonatomic) IBOutlet UILabel *cellTextLabel;


@property (weak, nonatomic) IBOutlet UIImageView *cellImage1;

@property (weak, nonatomic) IBOutlet UILabel *cellTextLabel1;


@end
