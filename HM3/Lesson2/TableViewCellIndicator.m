//
//  TableViewCellIndicator.m
//  Lesson2
//
//  Created by Мария Тимофеева on 08/10/15.
//  Copyright © 2015 Azat Almeev. All rights reserved.
//

#import "TableViewCellIndicator.h"

@implementation TableViewCellIndicator

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setActInd:(UIApplication *)indicator{

    UIApplication* indicatorActive = [UIApplication sharedApplication];
    indicatorActive.networkActivityIndicatorVisible = YES;
    
}


@end
