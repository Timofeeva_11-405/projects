//
//  PageViewController.m
//  Lesson2
//
//  Created by Мария Тимофеева on 10/10/15.
//  Copyright © 2015 Azat Almeev. All rights reserved.
//

#import "PageViewController.h"

@implementation PageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = self;
    NSMutableArray *viewControllers;
    [viewControllers addObject:[self.storyboard instantiateViewControllerWithIdentifier:@"Red"]];
    [viewControllers addObject:[self.storyboard instantiateViewControllerWithIdentifier:@"Green"]];
    [viewControllers addObject:[self.storyboard instantiateViewControllerWithIdentifier:@"Blue"]];
   [ self setViewControllers: viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
}
static int numberOfViewController = 1;
-(UIViewController *)presentViewController{
    UIViewController *currentController;
    if (numberOfViewController == 1){
        numberOfViewController++;
        currentController =  [self.storyboard instantiateViewControllerWithIdentifier:@"Red"];
    } else if (numberOfViewController ==2 ){
        numberOfViewController++;
        currentController =  [self.storyboard instantiateViewControllerWithIdentifier:@"Green"];
    } else{
        numberOfViewController = 1;
        currentController =  [self.storyboard instantiateViewControllerWithIdentifier:@"Blue"];
    }
    return currentController;
    
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    return self.presentViewController;
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    if(numberOfViewController <3){numberOfViewController++;
    }else {
        numberOfViewController = 1;
    }
    return self.presentViewController;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return 3;
}
- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController{
    return numberOfViewController++;
}
@end
