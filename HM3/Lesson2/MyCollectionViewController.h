//
//  MyCollectionViewController.h
//  Lesson2
//
//  Created by Мария Тимофеева on 11/10/15.
//  Copyright © 2015 Azat Almeev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectionViewController : UICollectionViewController
@property(nonatomic) NSMutableArray * items;
@property(nonatomic) NSMutableArray * cellIdentificators;
@end
