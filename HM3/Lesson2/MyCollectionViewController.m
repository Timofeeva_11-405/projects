//
//  MyCollectionViewController.m
//  Lesson2
//
//  Created by Мария Тимофеева on 11/10/15.
//  Copyright © 2015 Azat Almeev. All rights reserved.
//

#import "MyCollectionViewController.h"
#import "LoadingTableViewController.h"
#import "firstCell.h"
#import "secondCell.h"
#import "thirdCell.h"
#import "fourthCell.h"



@implementation MyCollectionViewController

- (NSMutableArray *)items{
    if(!_items)
        _items = [NSMutableArray new];
    return _items;
}
- (NSMutableArray *)cellIdentificators{
    _cellIdentificators = [NSMutableArray new];
    [_cellIdentificators addObject:[NSString stringWithFormat:@"firstCell"]];
    [_cellIdentificators addObject:[NSString stringWithFormat:@"secondCell"]];
    [_cellIdentificators addObject:[NSString stringWithFormat:@"thirdCell"]];
    [_cellIdentificators addObject:[NSString stringWithFormat:@"fourthCell"]];
    
    return _cellIdentificators;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    for(int i = 0;i <= 5;i++){
        NSMutableArray *subarray = [NSMutableArray new];
        [subarray addObject:[NSString stringWithFormat:@"%ld section", (long ) i]];
        [self.items addObject:subarray];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:_cellIdentificators[i]];
    }
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.items.count;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(!decelerate)[self loadDataUsingLastID:[NSNumber numberWithInteger:self.items.count]];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset <= 0)[self loadDataUsingLastID:[NSNumber numberWithInteger:self.items.count]];
}

- (void)didFailToLoadDataWithError:(NSError *)error {
    [self.collectionView reloadData];
    NSLog(@"the end");
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}
-(void)didLoadNewData:(NSArray *)items{
    int count = (int)self.items.count;
    
    UICollectionView *s = self.collectionView;
    
    for(int i = count+1;i < count + 6;i++){
        [self.collectionView beginUpdates];
        NSMutableArray *subarray = [NSMutableArray new];
        [subarray addObject:[NSString stringWithFormat:@"%ld section", (long ) i]];
        [self.items addObject:subarray];
        [self.collectionView reloadData];
        [s insertSections:[NSIndexSet indexSetWithIndex:count] ];
        [self.collectionView endUpdates];
    }
    [self.collectionView reloadData];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell;
    if (indexPath.row==0){
        firstCell *cell ;
        cell.textLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
    }
    if (indexPath.row==1){
        secondCell *cell ;
        cell.textLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
        cell.textSubtitle.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
    }
    if (indexPath.row==2){
        thirdCell *cell ;
        cell.textLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
        cell.textSubtitle.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
    }
    if (indexPath.row==3){
        fourthCell *cell ;
        cell.textLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
        cell.textSubtitle.text = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
    }
    
return cell;
}

@end
