//
//  thirdCell.h
//  Lesson2
//
//  Created by Мария Тимофеева on 11/10/15.
//  Copyright © 2015 Azat Almeev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface thirdCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *textSubtitle;


@end
