//
//  ViewController.m
//  HM1
//
//  Created by Мария Тимофеева on 29.09.15.
//  Copyright © 2015 ___matim___. All rights reserved.
//

#import "ViewController.h"
#import "MyClass.h"
#import "MyClass2.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    MyClass *class1 = [[MyClass alloc] init];
    [class1 sayHi];
    MyClass2 *class2 = [[MyClass2 alloc]init];
    [class2 sayMyAge];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
