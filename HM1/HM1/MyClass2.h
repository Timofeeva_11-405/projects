//
//  MyClass2.h
//  HM1
//
//  Created by Мария Тимофеева on 29.09.15.
//  Copyright © 2015 ___matim___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyClass2 : NSObject
@property NSString* _age;

-(void)sayMyAge;

@end
